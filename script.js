const player = document.getElementById("player");
let isJumping = false;
let isWalking = false;
let walkDirection = "right";

document.addEventListener("keydown", (event) => {
  if (event.code === "Space" && !isJumping) {
    jump();
  } else if (event.code === "ArrowLeft" && !isWalking) {
    walk("left");
  } else if (event.code === "ArrowRight" && !isWalking) {
    walk("right");
  }
});

function jump() {
  isJumping = true;
  let position = 0;

  const upInterval = setInterval(() => {
    if (position >= 150) {
      clearInterval(upInterval);

      const downInterval = setInterval(() => {
        if (position <= 0) {
          clearInterval(downInterval);
          isJumping = false;
        } else {
          position -= 10;
          player.style.bottom = position + "px";
        }
      }, 20);
    } else {
      position += 10;
      player.style.bottom = position + "px";
    }
  }, 20);
}

function walk(direction) {
  isWalking = true;
  walkDirection = direction;
  let position = parseInt(player.style.left);

  const walkInterval = setInterval(() => {
    if (direction === "right") {
      if (position >= 750) {
        clearInterval(walkInterval);
        isWalking = false;
      } else {
        position += 5;
        player.style.left = position + "px";
      }
    } else {
      if (position <= 0) {
        clearInterval(walkInterval);
        isWalking = false;
      } else {
        position -= 5;
        player.style.left = position + "px";
      }
    }
  }, 100);

  document.addEventListener("keyup", (event) => {
    if (
      (event.code === "ArrowLeft" && walkDirection === "left") ||
      (event.code === "ArrowRight" && walkDirection === "right")
    ) {
      clearInterval(walkInterval);
      isWalking = false;
    }
  });
}
// Add these lines after the player variable
const enemy = document.getElementById("enemy");
let isOverEnemy = false;

// Modify the walk function
function walk(direction) {
  isWalking = true;
  walkDirection = direction;
  let position = parseInt(player.style.left);

  const walkInterval = setInterval(() => {
    if (direction === "right") {
      if (position >= 750) {
        clearInterval(walkInterval);
        isWalking = false;
      } else {
        position += 5;
        player.style.left = position + "px";
        checkOverEnemy();
      }
    } else {
      if (position <= 0) {
        clearInterval(walkInterval);
        isWalking = false;
      } else {
        position -= 5;
        player.style.left = position + "px";
        checkOverEnemy();
      }
    }
  }, 100);

  // ... rest of the code
}

// Add a new function to check if the player is over the enemy
function checkOverEnemy() {
  const playerPosition = parseInt(player.style.left);
  const enemyPosition = parseInt(enemy.style.left);

  if (
    playerPosition + 50 > enemyPosition &&
    playerPosition < enemyPosition + 50 &&
    parseInt(player.style.bottom) <= 50
  ) {
    isOverEnemy = true;
    console.log("Player is over the enemy!");
  } else {
    isOverEnemy = false;
  }
}
// Add these variables after the existing ones
const background = document.getElementById("background");
const arrow = document.getElementById("arrow");
let arrowPosition = 800;
let gameSpeed = 5;

// Remove the walk function and the keydown event listener that calls it

// Add a new function for the game loop
function gameLoop() {
  // Move the background
  const backgroundPosition = parseInt(background.style.left) || 0;
  background.style.left = ((backgroundPosition - gameSpeed) % 800) + "px";

  // Move the player forward
  const playerPosition = parseInt(player.style.left);
  player.style.left = ((playerPosition + gameSpeed) % 800) + "px";

  // Move the arrow
  arrowPosition -= gameSpeed;
  if (arrowPosition <= -40) {
    arrowPosition = 800;
  }
  arrow.style.left = arrowPosition + "px";

  // Collision detection
  if (
    playerPosition + 50 > arrowPosition &&
    playerPosition < arrowPosition + 40 &&
    parseInt(player.style.bottom) <= 50
  ) {
    console.log("Game Over! Player hit the arrow.");
  }

  // Continue the game loop
  requestAnimationFrame(gameLoop);
}

// Call the game loop
gameLoop();
